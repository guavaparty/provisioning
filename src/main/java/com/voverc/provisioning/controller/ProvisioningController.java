package com.voverc.provisioning.controller;

import com.voverc.provisioning.service.ProvisioningService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@RestController
@RequestMapping("/api/v1")
public class ProvisioningController {
    @Autowired
    ProvisioningService provisioningService;

    @ResponseBody
    @GetMapping(value = "/provisioning/{macAddress}")
    public String getConfiguration(@PathVariable String macAddress) {
        return Optional.ofNullable(provisioningService.getProvisioningFile(macAddress))
                .orElseThrow(() ->
                        new ResponseStatusException(HttpStatus.NOT_FOUND,
                                String.format("device with mac address %s was not found", macAddress)));
    }
}