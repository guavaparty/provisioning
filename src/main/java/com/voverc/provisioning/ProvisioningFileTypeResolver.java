package com.voverc.provisioning;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.javaprop.JavaPropsMapper;
import com.voverc.provisioning.entity.Device;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;

public class ProvisioningFileTypeResolver {
    public static String getFile(Map<String, String> properties, Device device) throws JsonProcessingException {
        ObjectMapper mapper;
        switch (device.getModel()) {
            case DESK:
                mapper = new JavaPropsMapper();
                break;
            case CONFERENCE:
                mapper = new ObjectMapper();
                break;
            default:
                return null;
        }
        addProperties(properties, device.getOverrideFragment(), mapper);
        return mapper.writeValueAsString(properties);
    }

    @SuppressWarnings("unchecked")
    private static void addProperties(Map<String, String> properties, String overrideFragment, ObjectMapper mapper) throws JsonProcessingException {
        if (StringUtils.isEmpty(overrideFragment)) {
            return;
        }
        Map<String, String> additionalProperties = mapper.readValue(overrideFragment, HashMap.class);
        properties.putAll(additionalProperties);
    }
}
