package com.voverc.provisioning.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.voverc.provisioning.config.ApplicationProperties;
import com.voverc.provisioning.entity.Device;
import com.voverc.provisioning.model.ResponsePOJO;
import com.voverc.provisioning.repository.DeviceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ResponseStatusException;

import java.util.Map;

import static com.voverc.provisioning.ProvisioningFileTypeResolver.getFile;

@Service
public class ProvisioningServiceImpl implements ProvisioningService {
    @Autowired
    private DeviceRepository repository;
    @Autowired
    private ApplicationProperties applicationProperties;

    public String getProvisioningFile(String macAddress) {
        if (StringUtils.isEmpty(macAddress)) {
            return null;
        }
        Device device = repository.findById(macAddress).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                String.format("device with mac address %s was not found", macAddress)));
        try {
            Map<String, String> properties = collectProperties(device);
            return getFile(properties, device);
        } catch (JsonProcessingException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Internal Server Error", e);
        }
    }

    private Map<String, String> collectProperties(Device device) {
        ResponsePOJO responsePOJO = new ResponsePOJO();
        responsePOJO.setUsername(device.getUsername());
        responsePOJO.setPassword(device.getPassword());
        responsePOJO.setPort(applicationProperties.getPort());
        responsePOJO.setDomain(applicationProperties.getDomain());
        responsePOJO.setCodecs(applicationProperties.getCodecs());

        Map<String, String> properties =
                new ObjectMapper().convertValue(responsePOJO, new TypeReference<Map<String, String>>() {
                });

        return properties;
    }
}
