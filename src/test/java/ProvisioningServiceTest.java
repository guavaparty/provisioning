import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.javaprop.JavaPropsMapper;
import com.voverc.provisioning.ProvisioningApplication;
import com.voverc.provisioning.config.ApplicationProperties;
import com.voverc.provisioning.entity.Device;
import com.voverc.provisioning.repository.DeviceRepository;
import com.voverc.provisioning.service.ProvisioningService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

import static org.mockito.Mockito.when;

@SpringBootTest(classes = ProvisioningApplication.class)
@ActiveProfiles("test")
public class ProvisioningServiceTest {
    @Autowired
    private ProvisioningService provisioningService;
    @MockBean
    private DeviceRepository deviceRepository;
    @Autowired
    private ApplicationProperties applicationProperties;

    private static String macAddress;
    private static Device deskDevice;
    private static Device conferenceDevice;

    @BeforeAll
    static void setUp() {
        macAddress = "f1-e2-d3-c4-b5-a6";

        deskDevice = new Device();
        deskDevice.setMacAddress(macAddress);
        deskDevice.setModel(Device.DeviceModel.DESK);
        deskDevice.setPassword("1234");
        deskDevice.setUsername("user1");

        conferenceDevice = new Device();
        conferenceDevice.setMacAddress(macAddress);
        conferenceDevice.setModel(Device.DeviceModel.CONFERENCE);
        conferenceDevice.setPassword("1234");
        conferenceDevice.setUsername("user1");
    }

    @Test
    void shouldThrowExceptionWhenDeviceNotFound() {
        Assertions.assertThrows(ResponseStatusException.class, () -> {
            final String macAddress = "xx-xx-xx-xx";
            when(deviceRepository.findById(macAddress)).thenReturn(Optional.empty());
            String expected = provisioningService.getProvisioningFile(macAddress);
        });
    }

    @Test
    void testResponseForDeskDevice() throws JsonProcessingException {
        when(deviceRepository.findById(macAddress)).thenReturn(Optional.of(deskDevice));

        String expected = provisioningService.getProvisioningFile(macAddress);

        JsonNode node = new JavaPropsMapper().readTree(expected);

        Assertions.assertEquals(node.get("password").asText(), deskDevice.getPassword());
        Assertions.assertEquals(node.get("username").asText(), deskDevice.getUsername());
        Assertions.assertEquals(node.get("domain").asText(), applicationProperties.getDomain());
        Assertions.assertEquals(node.get("port").asText(), applicationProperties.getPort());
        Assertions.assertEquals(node.get("codecs").asText(), applicationProperties.getCodecs());
    }

    @Test
    void testResponseForConferenceDevice() throws JsonProcessingException {
        when(deviceRepository.findById(macAddress)).thenReturn(Optional.of(conferenceDevice));

        String expected = provisioningService.getProvisioningFile(macAddress);

        JsonNode node = new ObjectMapper().readTree(expected);

        Assertions.assertEquals(node.get("password").asText(), conferenceDevice.getPassword());
        Assertions.assertEquals(node.get("username").asText(), conferenceDevice.getUsername());
        Assertions.assertEquals(node.get("domain").asText(), applicationProperties.getDomain());
        Assertions.assertEquals(node.get("port").asText(), applicationProperties.getPort());
        Assertions.assertEquals(node.get("codecs").asText(), applicationProperties.getCodecs());
    }
}
