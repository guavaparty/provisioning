import com.voverc.provisioning.controller.ProvisioningController;
import com.voverc.provisioning.service.ProvisioningService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ContextConfiguration(classes = ProvisioningController.class)
@WebMvcTest(controllers = ProvisioningController.class)
public class ProvisioningControllerTest {
    @Autowired
    private MockMvc mockMVc;
    @MockBean
    private ProvisioningService provisioningService;

    @Test
    void shouldReturn404WhenNoDeviceFound() throws Exception {
        final String macAddress = "xx-xx-xx-xx";
        when(provisioningService.getProvisioningFile(macAddress)).thenReturn(null);
        this.mockMVc.perform(get("/api/v1/provisioning/{macAddress}", macAddress)).andExpect(status().isNotFound());
    }

    @Test
    void shouldReturn200IfDeviceFound() throws Exception {
        final String macAddress = "f1-e2-d3-c4-b5-a6";
        String provisioningFile = "{\"username\":\"sofia\",\"password\":\"red\",\"domain\":\"sip.voverc.com\",\"port\":\"5060\",\"codecs\":\"G711,G729,OPUS\"}";
        when(provisioningService.getProvisioningFile(macAddress)).thenReturn(provisioningFile);
        this.mockMVc.perform(get("/api/v1/provisioning/{macAddress}", macAddress)).andExpect(status().isOk());
    }
}
